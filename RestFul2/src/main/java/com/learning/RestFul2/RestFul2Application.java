package com.learning.RestFul2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestFul2Application {

	public static void main(String[] args) {
		SpringApplication.run(RestFul2Application.class, args);
	}

}
