package com.learning.beans;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.learning.utils.DateUtil;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameCatalog {
	private Integer			id;
	private String 			status;
	private LocalDateTime 	createdOn;
	private LocalDateTime	modifiedOn;
	private boolean       	activeFlag;
	
	//enum for game status
	public enum GameStatusType { P, D }
	
	//constants used with enum
	public static final String IN_PROGRESS 	= "Progress";
	public static final String DONE 		= "Done";
	
	//constructors
	public GameCatalog() {
	}

	public GameCatalog(Integer id, String status,  
					LocalDateTime createdOn, LocalDateTime modifiedOn,
					boolean activeFlag) {		
		this.id 		= id;			
		this.status 	= status;
		this.createdOn	= createdOn;
		this.modifiedOn	= modifiedOn;
		this.activeFlag	= activeFlag;
	}

	//setters and getters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public boolean isActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
	//end of setters and getters	
	
	/**
	 * @return
	 */
	public String getFormattedCreatedOn() {
		return DateUtil.toString(getCreatedOn());
	}
		
	public String getFormattedModifiedOn() {
		return DateUtil.toString(getModifiedOn());
	}	
	
	public String getTranslateGameStatus() {
		GameStatusType gst = GameStatusType.valueOf(getStatus());
		switch (gst) {
			case  P:
				return IN_PROGRESS;
			case  D:
				return DONE;
			default:
				return null;
		}					
	}	
}
