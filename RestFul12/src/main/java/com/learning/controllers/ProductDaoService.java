package com.learning.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.learning.beans.Product;

@Component //it allows to be managed by spring
public class ProductDaoService {
	private static Map<String, Product> productRepo = new HashMap<>();
	//private static List<Product> products = new ArrayList<>();
	static {
	      Product honey = new Product();
	      honey.setId("1");
	      honey.setName("Honey");
	      productRepo.put(honey.getId(), honey);
	      
	      Product almond = new Product();
	      almond.setId("2");
	      almond.setName("Almond");
	      productRepo.put(almond.getId(), almond);
	   }
	   
   //retrieve all products.
   public List<Product> findAllProducts(){
	   Collection<Product> values = productRepo.values();
	   return new ArrayList<>(values);
   }
   
   //update a product.
 	public Product updateProduct(Product product, String id) {
      productRepo.remove(id);
      product.setId(id);
      productRepo.put(id, product);
 		return product;
 	}
}
