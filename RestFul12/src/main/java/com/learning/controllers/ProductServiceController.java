package com.learning.controllers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.learning.beans.Product;
import com.learning.exceptions.ProductNotfoundException;

@RestController
public class ProductServiceController {
	@Autowired //allows Spring to inject the UserDaoService.
	private ProductDaoService service;
	private static Map<String, Product> productRepo = new HashMap<>();
	private static List<Product> products = new ArrayList<>();
	static {
      Product honey = new Product();
      honey.setId("1");
      honey.setName("Honey");
      productRepo.put(honey.getId(), honey);
      
      Product almond = new Product();
      almond.setId("2");
      almond.setName("Almond");
      productRepo.put(almond.getId(), almond);
   }
   
	@PutMapping("/products/{id}")
	public Product updateProduct(@PathVariable("id") String id, @RequestBody Product product){
		if(!productRepo.containsKey(id)) {
			throw new ProductNotfoundException(id);
		}
		product = service.updateProduct(product, id);
		return product;
	}
   
   @GetMapping("/allProducts")
   public ResponseEntity<Object> retrieveAllProducts() {
	   products = service.findAllProducts();
	   return new ResponseEntity<>(products, HttpStatus.OK);
	}
}
