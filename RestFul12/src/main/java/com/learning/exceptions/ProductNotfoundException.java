package com.learning.exceptions;

public class ProductNotfoundException extends RuntimeException {
	   private static final long serialVersionUID = 1L;
	   private String id;
	   
	   public ProductNotfoundException(String id) {
		   super();
		   this.id = id;
	   }

	   public String getId() {
		   return id;
	   }

	   public void setId(String id) {
		   this.id = id;
	   }	   	 
}
