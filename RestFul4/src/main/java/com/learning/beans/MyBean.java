package com.learning.beans;

public class MyBean {
	private String message;

	public MyBean(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return String.format("MyBean [message=%s]", message);
	}

	
}
