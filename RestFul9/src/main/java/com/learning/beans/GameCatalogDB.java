package com.learning.beans;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class GameCatalogDB implements Serializable, CommandLineRunner{
	private static final long serialVersionUID = 1L;
	
	@Autowired
    JdbcTemplate jdbcTemplate;	
	
	@Override
    public void run(String... args) throws Exception{		
	}
	
	////this retrieves a list of all GameCatalog objects
	public List<GameCatalog> getAllGameCatalog()  {
	    String sql = "select * from gameCatalog";
	     
	    List<GameCatalog> cat = jdbcTemplate.query(sql,
	                BeanPropertyRowMapper.newInstance(GameCatalog.class));	     	    
	    return cat;
	}
	
	public GameCatalog findOneGame(int id) {
	    String sql = "SELECT * FROM gameCatalog WHERE id = ?";
	    List<GameCatalog> cat = jdbcTemplate.query(sql, new Object[] {id},BeanPropertyRowMapper.newInstance(GameCatalog.class));
	    if(cat.size() == 0) {
	    	return null;
	    }	    
	    GameCatalog game = cat.get(0);
	    System.out.println(game);
	    return game;
	}
}
