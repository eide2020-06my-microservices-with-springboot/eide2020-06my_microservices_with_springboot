package com.learning.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.learning.beans.GameCatalog;
import com.learning.beans.GameCatalogDB;
import com.learning.exceptions.GameNotFoundException;

@RestController
public class SudokuController {
	@Autowired //allows Spring to inject the GameCatalogDB.
	private GameCatalogDB service;
	
	//to retrieve all games.
	@GetMapping("/allGames")
	public List<GameCatalog> retrieveAllGames() {
		return service.getAllGameCatalog();
	}
	
	/*to retrieve a specific game.
	 * it handles the game not found exception.
	 */
	@GetMapping("/game/{id}")
	public GameCatalog retrieveOneGame(@PathVariable int id) {
		GameCatalog game = service.findOneGame(id);
		if(game == null) {
			throw new GameNotFoundException("id: " + id);
		}
		return game;
	}
}

