package com.learning.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtil { 
	public static DateTimeFormatter OUT_TIMESTAMP_FORMAT            = DateTimeFormatter.ofPattern("M/d/yyyy h:mm:ss a");
	public static DateTimeFormatter OUT_TIMESTAMP_FORMAT_NO_SECONDS = DateTimeFormatter.ofPattern("M/d/yyyy h:mm a");
	public static DateTimeFormatter OUT_TIMESTAMP_FORMAT_NO_TIME	= DateTimeFormatter.ofPattern("M/d/yyyy");
	public static DateTimeFormatter OUT_TIMESTAMP_FORMAT_HH_MM      = DateTimeFormatter.ofPattern("h:mm a");
	public static DateTimeFormatter OUT_TIMESTAMP_FORMAT_FILE_NAME  = DateTimeFormatter.ofPattern("Mdyyyyhmmssa");
	
	
	/** enum for months
	 * 
	 */
	public enum MonthEnum {JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
						   JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER}
	
	/**
     * this formats a LocalDateTime object date + time in hh mm ss am/pm
     */
    public static String toString( LocalDateTime date )  {
        if( date == null ) return null;
               
        if( LocalDateTime.class.isAssignableFrom( date.getClass() ) )
        {
            return date.format(OUT_TIMESTAMP_FORMAT);
        }
        throw new IllegalArgumentException( "Unsupported type " + date.getClass() );
    }
    
    /**
     * this formats a LocalDateTime object date + time in hh mm; no time
     */
    public static String toString2( LocalDateTime date )  {
        if( date == null ) return null;
               
        if( LocalDateTime.class.isAssignableFrom( date.getClass() ) )
        {
            return date.format(OUT_TIMESTAMP_FORMAT_NO_TIME);
        }
        throw new IllegalArgumentException( "Unsupported type " + date.getClass() );
    }
    
    /**
     * this formats a LocalDateTime object date + time in hh mm ss am/pm;	
     */
    public static String toString3( LocalDateTime date )  {
        if( date == null ) return null;
               
        if( LocalDateTime.class.isAssignableFrom( date.getClass() ) )
        {
            return date.format(OUT_TIMESTAMP_FORMAT_HH_MM);
        }
        throw new IllegalArgumentException( "Unsupported type " + date.getClass() );
    }
    
    /**
     * this formats a LocalDateTime object date + time in hh mm am/pm; no seconds
     */
    public static String toString4( LocalDateTime date )  {
        if( date == null ) return null;
               
        if( LocalDateTime.class.isAssignableFrom( date.getClass() ) )
        {
            return date.format(OUT_TIMESTAMP_FORMAT_FILE_NAME);
        }
        throw new IllegalArgumentException( "Unsupported type " + date.getClass() );
    }
    
    /**
     * this formats a Date object date in "MM/dd/yyyy" to string
     */
    public static String toString5( Date date )  {
        if( date == null ) return null;
        
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");  
        return df.format(date);
    }
    
    /**
     * this formats a LocalDate object date in "yyy/mm/dd" to string
     * this hasn't been tested.
     */
    public static String toString6( LocalDate date )  {
        if( date == null ) return null;
        
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");  
        return df.format(date);
    }
    
	/**
     * Retrieves the today's date without time 
     */
    public static LocalDate newDate() {
    	return LocalDate.now();
    }
    
	/**
     * Retrieves the today's date with time in LocalDateTime 
    */
    public static LocalDateTime newDateTime() {
    	return LocalDateTime.now();
    }

	/**
     * Converts a LocalDateTime object in
     * a Timestamp object 
     */
    public static Timestamp convertLocalDateTimeToTimestamp(LocalDateTime dateTime) {
    	return Timestamp.valueOf(dateTime);
    }
    
    /**
     * Converts a Timestamp object in
     * a LocalDateTime object 
     */
    public static LocalDateTime convertTimestampToLocalDateTime(Timestamp dateTime) {
    	if( dateTime == null ) return null;
    	return dateTime.toLocalDateTime();
    }
    
    /**
     * Gets the days for the month in this date
     */
    public static int daysForThisMonth( LocalDate date )
    {       
        return date.lengthOfMonth();
    }
    
    /**
     * Gets the day in a week for this date from the enum using int
     */
    public static int dayInAWeek( LocalDate date )
    {       
    	int k = date.getDayOfWeek().getValue();
    	//convert from ISO-8601 to US format
    	switch (k) {
		  case 1: return 1;
		  case 2: return 2;
		  case 3: return 3;
		  case 4: return 4;
		  case 5: return 5;
		  case 6: return 6;
		  case 7: return 0;
		 }
    	return 0;
    }
    
    /**
     * Gets the first date for this Month-Year
     */
    public static LocalDate firstDateForThisMonthYear( LocalDate date )
    {       
        return LocalDate.of(date.getYear(), date.getMonth(), 1);
    }
    
    /**
     * Gets the month from this date. Returns Month object
     */
    public static Month monthOfThisDate( LocalDate date )
    {       
        return date.getMonth();
    }
    
    /**
     * Gets the month from this date. Returns int
     */
    public static int monthValueOfThisDate( LocalDate date )
    {       
        return date.getMonthValue();
    }
    
    /**
     * Gets the year from this date. Returns int
     */
    public static int yearOfThisDate( LocalDate date )
    {       
        return date.getYear();
    }
    
    /**
     * Gets the day from this date. Returns  
     */
    public static int dayOfThisDate( LocalDate date )
    {       
        return date.getDayOfMonth();
    }
    
    /**
     * Rolls the months for this date.  
     */
    public static LocalDate rollMonths( LocalDate date, int months )
    {       
        if (months > 0) {
        	return date.plusMonths(months);
        }
    	    return date.minusMonths(-months);
    }
    
    /**
     * Checks the day, month and year are equal.
     */
    public static boolean dateEquals( LocalDate d1, LocalDate d2 )
    {
       if( d1 == null || d2 == null ) return false;

       return dayOfThisDate( d1 )        == dayOfThisDate( d2 )        &&
    		  monthValueOfThisDate( d1 ) == monthValueOfThisDate( d2 ) &&
      		  yearOfThisDate( d1 )       == yearOfThisDate( d2 );
    }
    
	/**
     * Converts date to LocalDateTime 
    */
    public static LocalDateTime dateToLocalDateTime(Date date) {
    	Instant instant = Instant.ofEpochMilli(date.getTime());
    	LocalDateTime res = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    	return res;
    }
    
    /**
     * Converts LocalDateTime to date 
    */
    public static Date localDateTimeToDate(LocalDateTime date) {
    	Date res = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
    	return res;
    }
    
    /**
     * Builds a LocalDateTime object with year, month, and day 
    */
    public static LocalDateTime buildALocalTimeObject(int year, int monthOfYear, int dayOfMonth ) {
    	LocalDateTime res = LocalDateTime.of(year, monthOfYear, dayOfMonth, 0, 0);
    	return res;
    }
    
    /**takes a string containing the month name and converts it
     * to an Enum object
     */
    
    public static int buildAnEnumMonth(String month) {
       	int inMonth = 0;
    	MonthEnum monthE = MonthEnum.valueOf(month); 
    	switch (monthE) {
    		case  JANUARY:
    			inMonth = 1;
    			break;
    		case  FEBRUARY:
    			inMonth = 2;
    			break;
    		case  MARCH:
    			inMonth = 3;
    			break;
    		case  APRIL:
    			inMonth = 4;
    			break;
    		case  MAY:
    			inMonth = 5;
    			break;
    		case  JUNE:
    			inMonth = 6;
    			break;
    		case  JULY:
    			inMonth = 7;
    			break;
    		case  AUGUST:
    			inMonth = 8;
    			break;
    		case  SEPTEMBER:
    			inMonth = 9;
    			break;
    		case  OCTOBER:
    			inMonth = 10;
    			break;
    		case  NOVEMBER:
    			inMonth = 11;
    			break;
    		case  DECEMBER:
    			inMonth = 12;
    			break;
    	}
    	
    	return inMonth;
    }
    /**
     * Builds a LocalDateTime object with year, month, day, hours, minutes, and 00 seconds 
    */
    public static LocalDateTime buildALocalTimeObject(int year, int monthOfYear, int dayOfMonth, int hours, int minutes ) {
    	LocalDateTime res = LocalDateTime.of(year, monthOfYear, dayOfMonth, hours, minutes, 0);
    	return res;
    }
}
