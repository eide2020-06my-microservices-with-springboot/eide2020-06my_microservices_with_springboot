package com.learning.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.learning.beans.User;
import com.learning.beans.UserDaoService;
import com.learning.exceptions.UserNotFoundException;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
public class UserResourceController {
	@Autowired //allows Spring to inject the UserDaoService.
	private UserDaoService service;
	
	/*to retrieve all users.
	 * retrieves only id and name attributes.
	 */
	@GetMapping("/allUsers")
	public MappingJacksonValue retrieveAllUsers() {
		List<User> list = service.findAllUsers();
		
		//filter birthdate.
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter
				.filterOutAllExcept("id", "name");
		FilterProvider filters = new SimpleFilterProvider().addFilter("userFilter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(list);
		mapping.setFilters(filters);
		return mapping;
	}
	
	/*to retrieve a specific user.
	 * it handles the user not found exception.
	 * retrieves only name and birthdate attributes. 
	 */
	@GetMapping("/user/{id}")
	public MappingJacksonValue retrieveOneUser(@PathVariable int id) {
		User user = service.findOneUser(id);
		if(user == null) {
			throw new UserNotFoundException("id: " + id);
		}
		
		//filter id.
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter
										  .filterOutAllExcept("name", "birthdate");
		FilterProvider filters = new SimpleFilterProvider().addFilter("userFilter", filter);
		MappingJacksonValue mappedUser = new MappingJacksonValue(user);
		mappedUser.setFilters(filters);
		return mappedUser;
	}
	
	/*to create an user.
	 * this also returns the http created status 201 rather than
	 * the default 200.
	 * also the header contains the path of the created user.
	 */
//	@PostMapping("/user")
//	public void createUser(@RequestBody User user) {
//		User savedUser = service.saveUser(user);
//	}
	
	@PostMapping("/user")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = service.saveUser(user);
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();
				
		return ResponseEntity.created(location).build();
	}
	
	/*to delete a user.
	 * it handles the user not found exception.
	 */
	@DeleteMapping("/user/{id}")
	public void deleteAnUser(@PathVariable int id) {
		User user = service.deleteAnUser(id);
		if(user == null) {
			throw new UserNotFoundException("id: " + id);
		}
	}
}
