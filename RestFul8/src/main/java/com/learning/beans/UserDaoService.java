package com.learning.beans;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;
/*
working with POST, the goal is to create an user.
the saveUser() only creates the user, it doesn't send a result status.
* to get that postman works you need to add into the headers:
* 	Content-Type.........application-json	 
*/
@Component //it allows to be managed by spring
public class UserDaoService {
	private static List<User> users = new ArrayList<>();
	private static int userCount = 5;
	
	static {
//		users.add(new User(1, "Lina Maria", new Date()));
		users.add(new User(1, "Lina Maria", new GregorianCalendar(1981, 1, 26).getTime()));	
		users.add(new User(2, "Diana Jimena", new GregorianCalendar(1984, 5, 27).getTime())); 
		users.add(new User(3, "Nicholas", new GregorianCalendar(2013, 0, 4).getTime())); 
		users.add(new User(4, "Sophia", new GregorianCalendar(2018, 8, 6).getTime()));
		users.add(new User(5, "Patrick", new GregorianCalendar(2019, 10, 28).getTime()));
		 
	}
	
	//retrieve all users.
	public List<User> findAllUsers(){
		return users;
	}
	
	//save (or create) an user.
	public User saveUser(User user) {
		if(user.getId() == null) {
			user.setId(++userCount);
		}
		users.add(user);
		return user;
	}
	
	//retrieve a specific user.
	public User findOneUser(int id) {
		for(User user : users) {
			if(user.getId() == id) {
				return user;
			}
		}
		return null;
	}
	
	//delete a user.
		public User deleteAnUser(int id) {
			Iterator<User> iterator = users.iterator();
			while(iterator.hasNext()) {
				User user = iterator.next();
				if(user.getId() == id) {
					iterator.remove();
					return user;
				}
			}			
			return null;
		}
}
