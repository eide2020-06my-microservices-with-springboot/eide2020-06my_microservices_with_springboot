package com.learning.RestFul3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestFul3Application {

	public static void main(String[] args) {
		SpringApplication.run(RestFul3Application.class, args);
	}

}
