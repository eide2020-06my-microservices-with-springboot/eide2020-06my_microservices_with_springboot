package com.learning.RestFul3
.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*this class implements a service
 * that returns a string. But
 * it has an improvement in the mapping annotation.
 * Since the HelloWorldController class defined 
 * /hello-world as uri, I need to use a different one.
 * The method name could be the same for both classes.
 */
@RestController
public class HelloWorldControllerB {
	@GetMapping(path = "/hello-worldb")
	public String helloWorld() {
		return "Hello World with improvement!!!!";
	}
}
