package com.learning.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*
 * the response comes in a json format.
 * it has a kind of two levels:
 * first level (Quote class):
 * 		type (string)
 * 		value (object)
 * second level is the expansion of the value object (value class):
 * 		id (long)
 * 		quote (string). 
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {

  private String type;
  private Value value;

  public Quote() {
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Value getValue() {
    return value;
  }

  public void setValue(Value value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "Quote{" +
        "type='" + type + '\'' +
        ", value=" + value +
        '}';
  }
}
