package com.learning.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.learning.beans.User;
import com.learning.beans.UserDaoService;
import com.learning.exceptions.UserNotFoundException;

@RestController
public class UserResourceController {
	@Autowired //allows Spring to inject the UserDaoService.
	private UserDaoService service;
	
	//to retrieve all users.
	@GetMapping("/allUsers")
	public List<User> retrieveAllUsers() {
		return service.findAllUsers();
	}
	
	/*to retrieve a specific user.
	 * it handles the user not found exception.
	 */
	@GetMapping("/user/{id}")
	public User retrieveOneUser(@PathVariable int id) {
		User user = service.findOneUser(id);
		if(user == null) {
			throw new UserNotFoundException("id: " + id);
		}
		return user;
	}
	
	/*to create an user.
	 * this also returns the http created status 201 rather than
	 * the default 200.
	 * also the header contains the path of the created user.
	 */
//	@PostMapping("/user")
//	public void createUser(@RequestBody User user) {
//		User savedUser = service.saveUser(user);
//	}
	
	@PostMapping("/user")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = service.saveUser(user);
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();
				
		return ResponseEntity.created(location).build();
	}
	
	/*to delete a user.
	 * it handles the user not found exception.
	 */
	@DeleteMapping("/user/{id}")
	public void deleteAnUser(@PathVariable int id) {
		User user = service.deleteAnUser(id);
		if(user == null) {
			throw new UserNotFoundException("id: " + id);
		}
	}
}
