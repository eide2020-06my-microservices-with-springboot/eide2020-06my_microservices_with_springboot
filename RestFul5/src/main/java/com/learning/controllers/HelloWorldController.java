package com.learning.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/*helloWorld(): returns a bean property (a string). 
 * Since the HelloWorldController class defined 
 * /hello-world as uri, I need to use a different one.
 * The method name could be the same for both classes.
 * helloWorld_parm(): also returns a bean property (a string). 
 * but it uses a parameter to be appended to the answer.
 */
@RestController
public class HelloWorldController {		
	@GetMapping(path = "/hello-world-from-a-bean-with-parameter/{name}")
	public void helloWorld_parm(@PathVariable String name) {
		//return new MyBean("Hello my friend,  " + name); or
	}
}
